from bge import logic

def main():
	pass

def mouseShow():
	render.showMouse(1)

def mouseHide():
	render.showMouse(0)

def track(cont):
	scene = logic.getCurrentScene()
	#obj   = cont.getOwner()
	mOver = cont.sensors["mOver"]

	if mOver.positive:
		tracker = scene.objects["cursor3d"]
		(x, y, z) = mOver.hitPosition
		from HexGrid import pix2hex
		c = pix2hex(x, y)
		(x1, y1) = c.toPix()
		tracker["x"] = x1
		tracker["y"] = y1
		tracker["q"] = c.q
		tracker["r"] = c.r
		tracker.worldPosition = (x1, y1, 0)

		#tracker.worldPosition = (x - x % 2 + 1, y - y % 2 + 1, z + 1)

def add(cont):
	scene = logic.getCurrentScene()
	#curs = scene.objects["obj"]
	o = scene.addObject("obj", "cursor3d", 0)

def move(cont):
	if cont.sensors["MLB"].positive:
		scene = logic.getCurrentScene()
		unit = scene.objects["unit1"]
		tracker = scene.objects["cursor3d"]
		(x, y, z) = tracker.worldPosition
		unit.worldPosition = (x,y,z)
		unit["q"] = tracker["q"]
		unit["r"] = tracker["r"]

def showPath(cont):
	scene = logic.getCurrentScene()
	obj = cont.owner
	path = obj["path"]
	cur = scene.objects["cursor3d"]
	(cq, cr) = (cur["q"], cur["r"])
	from HexGrid import AxialCoord
	unit = scene.objects["unit1"]
	line = AxialCoord(unit["q"], unit["r"]).lineTo(AxialCoord(cq,cr))
	for c in path:
		(o, (q,r)) = c
		o.visible = False
	
	for n in range(0, len(line)):
		(o, (q,r)) = path[n]
		(x, y) = line[n].toPix()
		o.worldPosition = (x, y, 0)
		o.visible = True
