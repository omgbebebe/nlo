from bge import logic
from math import sin, cos
from mathutils import Vector

def camSphere(cont):
	own = cont.owner
	sc  = logic.getCurrentScene()
	tgt = sc.objects["camTarget"]
	if tgt:
		tgtPos = tgt.worldPosition
		r = own["r"]
		t = own["theta"]
		p = own["phi"]
		currPos = own.worldPosition
		newPos = sphereCoord(tgtPos, r, t, p)
		own.worldPosition = currPos.lerp(newPos, 0.1)
		# point the cameras 'Y' and use its 'Z' as up
		q = (tgt.worldPosition - newPos).to_track_quat('Y', 'Z')
		own.worldOrientation = q.to_matrix()

def sphereCoord(p0: Vector, r: float, theta: float, phi: float) -> Vector:
	cosT = cos(theta)
	sinP = sin(phi)
	(x0, y0, z0) = p0
	return Vector((x0+r*cosT*sinP, y0+r*sin(theta)*sinP, z0+r*cos(phi)))
