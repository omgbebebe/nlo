from math import sqrt

size = 1

class CubeCoord(object):
	def __init__(self, x: int, y: int, z: int):
		self.x = x
		self.y = y
		self.z = z
		self.size = size

	def toAxial(self):
		return AxialCoord(self.x, self.z)

	#def dist(self, to: CubeCoord) -> int:
	def dist(self, to) -> int:
		x1 = self.x
		y1 = self.y
		z1 = self.z
		x2 = to.x
		y2 = to.y
		z2 = to.z
		return max(abs(x1-x2), abs(y1-y2), abs(z1-z2))


	def round(self):
		rx = round(self.x)
		ry = round(self.y)
		rz = round(self.z)

		xDiff = abs(rx - self.x)
		yDiff = abs(ry - self.y)
		zDiff = abs(rz - self.z)

		if xDiff > yDiff and xDiff > zDiff:
			rx = -ry-rz
		elif yDiff > zDiff:
			ry = -rx-rz
		else:
			rz = -rx-ry

		self.x = rx
		self.y = ry
		self.z = rz
		return CubeCoord(rx, ry, rz)

class AxialCoord(object):
	def __init__(self, q: int, r: int):
		self.q = q
		self.r = r
		self.size = size

	def __add__(self, b):
		#self.q += b.q
		#self.r += b.r
		#return self
		return AxialCoord(self.q + b.q, self.r + b.r)

	def __mul__(self, x):
		#self.q *= x
		#self.r *= x
		return AxialCoord(self.q*x, self.r*x)

	def toCube(self):
		x = self.q
		z = self.r
		y = (-x)-z
		return CubeCoord(x, y, z)

	def dist(self, to) -> int:
		q1 = self.q
		r1 = self.r
		q2 = to.q
		r2 = to.r
		return int((abs(q1-q2) + abs(r1-r2) + abs(q1+r1-q2-r2)) / 2)

	def toPix(self):
		q = self.q
		r = self.r
		x = self.size * sqrt(3) * (q + r/2)
		y = self.size * 3/2 * r
		return (x, y)

	def lineTo(self, to):
		dist = self.dist(to)
		res = []
		for n in range(0,dist):
			c = self * (1-n/dist) + to * (n/dist)
			p = c.toCube().round().toAxial()
			res.append(p)
		return res
		


def pix2hex(x: int, y: int) -> AxialCoord:
	q = (1/3 * sqrt(3) * x - 1/3 * y) / size
	r = 2/3 * y / size
	res = (AxialCoord(q, r).toCube().round().toAxial())
	#print (x, y, res.q, res.r)
	return res



#q1 = CubeCoord(10,0,0)
#q2 = CubeCoord(3,5,0)

#print (q1.dist(q2))

#a1 = q1.toAxial()
#a2 = q2.toAxial()
#print (a1.dist(a2))

#mx = 10
#my = 20
#print (mx, my)
#qq = pix2hex(mx, my).toCube().round()
#aa = qq.toAxial()
#print (qq.x, qq.y, qq.z)
#print (aa.toPix())
